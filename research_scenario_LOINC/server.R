library(ggplot2)
library(gridExtra)
library(reshape2)

start.time=Sys.time()
source("data/biome2loinc_simulator.R")
PtData=gen_data(biome2loinc = TRUE)


multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  numPlots = length(plots)
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  if (numPlots==1) {
    print(plots[[1]])
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

shinyServer(function(input, output, session) {
  autoInvalidate=reactiveTimer(60000,session)
  
  # Fill in the spot we created for a plot
  output$tempPlot <- renderPlot({
    autoInvalidate()
    
    #Extract 
    PtDat=PtData[timing<Sys.time()]
    
    # Extract location of interest
    PtDat=data.frame(PtDat[location==input$patient])
    colnames(PtDat)=gsub("X","",colnames(PtDat))
    colnames(PtDat)=gsub("\\.","-",colnames(PtDat))
    #PtDat=data.frame(PtDat[location=="Inpatient_unit_A"])
    
    sparklines <- function(variable="HR", top=FALSE, bottom=FALSE){
      localenv <- environment()
      #variable="32290-9_ACETONE"
      ymax=max(PtDat[,variable])
      tmax=PtDat[which(PtDat[,variable]==ymax),"timing"]
      ymin=min(PtDat[,variable])
      tmin=PtDat[which(PtDat[,variable]==ymin),"timing"]
      
      p=ggplot(PtDat, aes(xmin=min(timing), xmax=max(timing), ymin=ymin,ymax=ymax), environment=localenv)+
      #p=ggplot(PtDat, aes(xmin=min(timing), xmax=max(timing), ymin=ymin,ymax=ymax))+
        geom_line(size=1, aes(y=PtDat[,variable], x=PtDat[,"timing"], colour=ID))+
        theme(plot.margin=unit(c(-.25,-.5,0,.425),"cm"),legend.position="none",panel.grid.minor=element_blank(),
              panel.grid.major=element_blank(),panel.border=element_blank(),panel.background=element_blank())+
        ylab(variable)+
        xlab("Date_Time")+
        scale_y_continuous(limits=c(ymin,ymax))+
        geom_point(size=4,colour="blue",aes(x=tmax[1],y=ymax))+  
        geom_point(size=4,colour="red",aes(x=tmin[1],y=ymin))  
      if(top){p=p+theme(legend.position="top")}  
      if(!bottom){p=p+theme(axis.title.x=element_blank(),axis.text.x=element_blank(),axis.ticks.x=element_blank())}
      p
      }# end sparklines fxn
    
    #inputs=c("ALBUMIN","A.G.Ratio","ACETONE","ALDOLASE")
            
    drawCovs <- function(cd, ncols) {
      require(ggplot2)
      require(reshape2)
      plots=list()
      #cmat=list()
      plots[[1]]=sparklines(variable=input$checkGroup[1],top=TRUE)
      for (i in 2:(length(cd$covmat)-1)) {
        #cmat[[i]] <- cd$covmat[[i]]
        plots[[i]] <- sparklines(variable=input$checkGroup[i])
      }
      plots[[length(cd$covmat)]]=sparklines(variable=input$checkGroup[length(cd$covmat)],bottom=TRUE)
      multiplot(plotlist=plots,cols=ncols)
    }
    
    cd <- list()
    cd$covmat <- input$checkGroup     ##################### Input list of desired health features
    
    if (length(input$checkGroup)==1){sparklines(variable=input$checkGroup[1],top=TRUE,bottom=TRUE)}
    #else if(length(input$checkGroup)==2){}
    else{drawCovs(cd, as.numeric(input$cols))}
  }, width=1400, height=800) # End renderPlot
})